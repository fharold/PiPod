//
// Created by fritsch on 26/02/18.
//

#ifndef PIPOD_FILETYPE_H
#define PIPOD_FILETYPE_H

enum FileType
{
    TYPE_FILE = 0,
    TYPE_DIRECTORY = 1,
    TYPE_NA = 2
};

#endif //PIPOD_FILETYPE_H
