//
// Created by fritsch on 27/02/18.
//

#ifndef PIPOD_MUSICSERVICE_H
#define PIPOD_MUSICSERVICE_H


#include <mpd/client.h>
#include <mpd/status.h>
#include <mpd/entity.h>
#include <mpd/search.h>
#include <mpd/tag.h>
#include <mpd/message.h>

#include "Service.h"

class MusicService : Service
{
public:
    MusicService();
    void playMusicAtGivenPath(const char *path);

    void start() override;

    void decreaseVolumeClicked();

    void increaseVolumeClicked();

    void playPreviousSongClicked();

    void playNextSongClicked();

    void playClicked();

private:
    void stop() override;
    struct mpd_connection *connection;
};


#endif //PIPOD_MUSICSERVICE_H
