//
// Created by fritsch on 27/02/18.
//

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include "MusicService.h"

static int handle_error(struct mpd_connection *c)
{
    assert(mpd_connection_get_error(c) != MPD_ERROR_SUCCESS);

    fprintf(stderr, "%s\n", mpd_connection_get_error_message(c));
    mpd_connection_free(c);
    return EXIT_FAILURE;
}

void MusicService::start()
{
    this->connection = mpd_connection_new(nullptr, 0, 30000);
    if (mpd_connection_get_error(this->connection) != MPD_ERROR_SUCCESS)
    {
        handle_error(connection);
        exit(-1);
    }
    for (int i = 0; i < 3; i++)
    {
        printf("version[%i]: %i\n",i,
               mpd_connection_get_server_version(this->connection)[i]);
    }
    mpd_status *status;
}

void MusicService::stop()
{

}

void MusicService::playMusicAtGivenPath(const char *path)
{
}

MusicService::MusicService()
{

}

void MusicService::decreaseVolumeClicked()
{

}

void MusicService::increaseVolumeClicked()
{

}

void MusicService::playPreviousSongClicked()
{

}

void MusicService::playNextSongClicked()
{

}

void MusicService::playClicked()
{

}
