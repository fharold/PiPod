//
// Created by fritsch on 28/02/18.
//

#ifndef PIPOD_MUSICEXTENSION_H
#define PIPOD_MUSICEXTENSION_H


enum MusicExtension
{
    MP3,
    FLAC,
    OTHER
};


#endif //PIPOD_MUSICEXTENSION_H
